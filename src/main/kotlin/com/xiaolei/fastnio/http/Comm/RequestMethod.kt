package com.xiaolei.fastnio.http.Comm

/**
 * 请求方式
 */
enum class RequestMethod
{
    GET, POST, PUT, DELETE
}