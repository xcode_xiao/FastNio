package com.xiaolei.fastnio.http.Comm

import com.xiaolei.fastnio.http.HttpSession
import java.util.concurrent.ConcurrentHashMap

/**
 * 保存Http请求会话的库
 */
object HttpSessionStore 
{
    private val sessions = ConcurrentHashMap<String, HttpSession>()
     fun getOrCreateSession(sessionId: String): HttpSession
    {
        var session = sessions[sessionId]
        if (session == null)
        {
            session = HttpSession(sessionId)
            sessions[sessionId] = session
        }
        return session
    }
}