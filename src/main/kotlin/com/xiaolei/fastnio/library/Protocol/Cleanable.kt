package com.xiaolei.fastnio.library.Protocol

/**
 * 可清理的
 */
interface Cleanable
{
    // 清理的回调
    fun onClear()
}