package com.xiaolei.fastnio.http

import java.io.Serializable
import java.util.HashMap

/**
 * 网络会话
 */
class HttpSession(val sessionId: String) : HashMap<String, Serializable>()
{
    fun setAttribute(key: String, value: Serializable)
    {
        this[key] = value
    }
}