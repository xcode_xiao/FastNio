package com.xiaolei.fastnio.http.Comm

/**
 * Http的响应状态
 */
enum class HttpStatus(val code: Int)
{
    OK(200),
    NOT_FOUND(404),
    SERVER_ERROR(500),
    METHOD_NOT_ALLOW(405),
    REDIRECT(302),
}