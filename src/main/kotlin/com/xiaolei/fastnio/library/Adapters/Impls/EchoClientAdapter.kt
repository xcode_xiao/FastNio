package com.xiaolei.fastnio.library.Adapters.Impls

import com.xiaolei.fastnio.library.Client.Session
import com.xiaolei.fastnio.library.Protocol.Impls.StringProtocol

/**
 * 在控制台输出文字的适配器
 */
class EchoClientAdapter : ProtocolClientAdapter<String, StringProtocol>(StringProtocol::class)
{
    override fun onSessionCreate(session: Session)
    {
        println("打开会话:${session.sessionId},${Thread.currentThread().name}")
    }

    override fun onSessionReceive(session: Session, data: String)
    {
        println("读取会话:${data},${Thread.currentThread().name}")
        session.write("服务端已收到:$data".toByteArray())
    }

    override fun onSessionClose(session: Session)
    {
        println("关闭会话:${session.sessionId},${Thread.currentThread().name}")
    }
}