package com.xiaolei.fastnio.http.Ext

infix fun <T : Any> T.ifTrue(isTrue: T.(obj: T) -> Boolean): ObjWrap<T>
{
    val result = this.isTrue(this)
    return ObjWrap(result, this)
}

infix fun <T : Any> ObjWrap<T>.elseOr(elseOr: T): T
{
    if (result)
    {
        return tag
    }
    return elseOr
}

class ObjWrap<T : Any>(val result: Boolean, val tag: T)