package com.xiaolei.fastnio.http.Comm

/**
 * 用来获取contentType的数据
 */
object ContentType
{
    const val html = "text/html"
    const val js = "application/javascript"
    const val css = "text/css"
    const val xml = "text/xml"
    const val gif = "image/gif"
    const val jpg = "image/jpeg"
    const val png = "image/png"
    const val json = "application/json"

    // 默认的
    const val octStream = "application/octet-stream"

    const val default = html

    fun getContentType(extends: String): String
    {
        return when (extends.toUpperCase())
        {
            "HTML" -> html
            "JS" -> js
            "CSS" -> css
            "XML" -> xml
            "GIF" -> gif
            "JPG", "JPEG" -> jpg
            "PNG" -> png
            else -> default
        }
    }
}