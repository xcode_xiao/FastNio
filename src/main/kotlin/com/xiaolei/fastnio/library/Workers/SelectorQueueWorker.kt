package com.xiaolei.fastnio.library.Workers

import java.nio.channels.SelectionKey
import java.nio.channels.Selector
import java.util.concurrent.LinkedBlockingQueue

/**
 * 关注客户端监听感兴趣的事件，然后把客户端放置到任务队列中
 * 由处理队列的线程进行统一去处理
 */
class SelectorQueueWorker(private val selector: Selector,
                          private val queue: LinkedBlockingQueue<SelectionKey>) : Runnable
{
    override fun run()
    {
        while (selector.isOpen)
        {
            val count = selector.select()
            if (count > 0)
            {
                val iterator = selector.selectedKeys().iterator()
                while (iterator.hasNext())
                {
                    val key = iterator.next()
                    iterator.remove()
                    // 先取消注册，避免多个线程同时为一个用户服务
                    key.cancel()
                    // 往队列里塞数据，如果队列已满，则阻塞
                    queue.put(key)
                }
            }
        }
    }
}