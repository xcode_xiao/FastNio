package com.xiaolei.fastnio.library.Factorys.Impls

import com.xiaolei.fastnio.library.Adapters.IClientAdapter
import com.xiaolei.fastnio.library.Factorys.IClientAdapterFactory
import kotlin.reflect.KClass
import kotlin.reflect.full.createInstance

/**
 * 单例的客户端适配器
 */
class SingleClientAdapterFactory<T : IClientAdapter>(private val klass: KClass<T>) : IClientAdapterFactory<T>
{
    private val adapter by lazy {
        klass.createInstance()
    }
    override fun get(ssId: String) = adapter
    override fun has(ssId: String) = true
    override fun remove(ssId: String) = Unit
}