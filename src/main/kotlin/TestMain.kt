import com.xiaolei.fastnio.http.http
import java.io.File
import java.util.*

fun main()
{
    http {

        // 设置端口
        port = 8000
        // 普通网页接口
        get("/") { request, response ->
            val lastTime = request.httpSession["lastTime"]
            response.writeHtml("<html><body>Hello World 上次时间:$lastTime</body></html>")
            request.httpSession["lastTime"] = Date()
        }

        // 下载文件接口
        get("download") { _, response ->
            response.sendRedirect("/")
        }

        // 设置静态文件夹
        static("/Users/xiaolei/IdeaProjects/FastNio/src/main/resources")
        
        // 设置首页的Uri
        setIndex("/index.html")
        
        // 设置临时文件夹
        tempDir = File("/Users/xiaolei/IdeaProjects/FastNio/src/main/resources/tmp")
    }
}