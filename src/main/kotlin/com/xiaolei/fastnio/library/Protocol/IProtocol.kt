package com.xiaolei.fastnio.library.Protocol

import com.xiaolei.fastnio.library.Client.Session

/**
 * 自定义协议的接口
 */
interface IProtocol<T> : Cleanable
{
    /**
     * 自定义协议的解码函数
     * @param session 会话的句柄
     * @param byteArray 传入的字节数组
     * @param len 字节数组的可读长度
     * @return 返回的自定义协议的实体，如果没有解析完，则返回 null
     */
    fun decode(session: Session, byteArray: ByteArray, len: Int): T?
}