package com.xiaolei.fastnio.library.Adapters

import com.xiaolei.fastnio.library.Client.Session

/**
 * 要对连接进来的客户端进行管理，实现这个接口，然后设置到NioServer里去
 */
interface IClientAdapter
{
    /**
     * 打开会话
     */
    fun onOpen(session: Session)

    /**
     * 读取数据
     */
    fun onReceive(session: Session, byteArray: ByteArray, len: Int)

    /**
     * 关闭会话
     */
    fun onClose(session: Session)
}