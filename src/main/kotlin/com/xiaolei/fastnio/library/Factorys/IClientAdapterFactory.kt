package com.xiaolei.fastnio.library.Factorys

import com.xiaolei.fastnio.library.Adapters.IClientAdapter

/**
 * 定义一个统一的生成器接口，
 * 这里由调用者自由控制，
 * 可以所有用户公用一个客户端，也可以所有用户每人一个客户端
 */
interface IClientAdapterFactory<T : IClientAdapter>
{
    /**
     * 获取一个ClientAdapter
     */
    fun get(ssId: String): T

    /**
     * 检测这个ClientAdapter是否存在
     */
    fun has(ssId: String): Boolean

    /**
     * 移除这个ClientAdapter
     */
    fun remove(ssId: String)
}