package com.xiaolei.fastnio.library.Protocol.Impls

import com.xiaolei.fastnio.library.Client.Session
import com.xiaolei.fastnio.library.Protocol.IProtocol

/**
 * 字符串的协议解码器
 */
class StringProtocol : IProtocol<String>
{
    /**
     * 自定义协议的解码函数
     * @param session 会话的句柄
     * @param byteArray 传入的字节数组
     * @param len 字节数组的可读长度
     * @return 返回的自定义协议的实体，如果没有解析完，则返回 null
     */
    override fun decode(session: Session, byteArray: ByteArray, len: Int): String?
    {
        return String(byteArray, 0, len)
    }

    override fun onClear()
    {
        
    }
}